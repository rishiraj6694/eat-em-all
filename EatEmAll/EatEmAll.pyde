
def createDebris(N, maxMass, maxVel):
    debrisX = [random(-w/2, w/2) for _ in range(N)]
    debrisY = [random(-h/2, h/2) for _ in range(N)]
    debrisVelX = [random(-maxVel, maxVel) for _ in range(N)]
    debrisVelY = [random(-maxVel, maxVel) for _ in range(N)]
    debrisMass = [random(5, maxMass) for _ in range(N)]
    return debrisX, debrisY, debrisVelX, debrisVelY, debrisMass

def createAliens(N, maxMass):
    alienX = [random(-w/2, w/2) for _ in range(N)]
    alienY = [random(-h/2, h/2) for _ in range(N)]
    alienMass = [random(5, maxMass) for _ in range(N)]
    return alienX, alienY, alienMass

w, h = 900, 600
levelNum, timeElapsed = 1, 0

mass = 10

numRed, numGold = 5, 0
maxMass = 15
maxVel = 2.5
debrisX, debrisY, debrisVelX, debrisVelY, debrisMass = createDebris(numRed, maxMass, maxVel)
alienX, alienY, alienMass = createAliens(numGold, maxMass)
alienVel = 1.0

gameOver = False

def setup():
    size(w, h)
    noCursor()
    font = createFont("Zapfino", 18)
    textFont(font)
    
def draw():
    global timeElapsed
    background(0)
    translate(w/2, h/2)

    if len(debrisX) + len(alienX) == 0:
        levelUp()
    drawDebris()
    drawAliens()
    drawSelf()
    if timeElapsed < 200:
        countDown()
    else:
        checkCollisions()
        moveDebris()
        moveAliens()
    text("LEVEL " + str(levelNum), -50, -2*h/5)
    timeElapsed += 1
    
def levelUp():
    global levelNum, timeElapsed, numRed, numGold, maxMass, maxVel
    global mass, debrisX, debrisY, debrisVelX, debrisVelY, debrisMass
    global alienX, alienY, alienVel, alienMass
    levelNum += 1
    if levelNum <= 4:
        numRed *= 1.5
        maxMass += 5
        maxVel += 0.5
    else:
        numRed *= 1.1
        numGold = (levelNum - 5)**2 + 1
        maxMass += 5
        alienVel += 0.25
    mass = 10 + levelNum
    timeElapsed = 0
    debrisX, debrisY, debrisVelX, debrisVelY, debrisMass = createDebris(int(numRed), maxMass, maxVel)
    alienX, alienY, alienMass = createAliens(int(numGold), maxMass)
    
def drawDebris():
    fill(255, 0, 0)
    for i in range(len(debrisX)):
        x, y, m = debrisX[i], debrisY[i], debrisMass[i]
        ellipse(x, y, m, m)
        
def drawAliens():
    fill(255, 200, 50)
    for i in range(len(alienX)):
        x, y, m = alienX[i], alienY[i], alienMass[i]
        ellipse(x, y, m, m)
        
def drawSelf():
    x, y = mouseX - w/2, mouseY - h/2
    fill(0, 255, 0)
    ellipse(x, y, mass, mass)
    
def moveDebris():
    global debrisX, debrisY, debrisVelX, debrisVelY
    for i in range(len(debrisX)):
        x, y, vx, vy, m = debrisX[i], debrisY[i], debrisVelX[i], debrisVelY[i], debrisMass[i]
        if x + vx <= -w/2 or x + vx >= w/2:
            debrisVelX[i] *= -1
        if y + vy <= -h/2 or y + vy >= h/2:
            debrisVelY[i] *= -1
        debrisX[i] += vx
        debrisY[i] += vy
        
def moveAliens():
    global alienX, alienY
    for i in range(len(alienX)):
        x, y, m = alienX[i], alienY[i], alienMass[i]
        myX, myY = mouseX - w/2, mouseY - h/2
        vx, vy = alienVel * sign(myX - x), alienVel * sign(myY - y)
        if alienMass[i] > mass:
            alienX[i] += vx
            alienY[i] += vy
        elif dist(x, y, myX, myY) < w/4:
            alienX[i] -= 2*vx
            alienY[i] -= 2*vy
        
        if alienX[i] > w/2:
            alienX[i] = w/2
        if alienX[i] < -w/2:
            alienX[i] = -w/2
        if alienY[i] > h/2:
            alienY[i] = h/2
        if alienY[i] < -h/2:
            alienY[i] = -h/2
        
def countDown():
    if timeElapsed < 50:
        text("3", 0, 0)
    elif timeElapsed < 100:
        text("2", 0, 0)
    elif timeElapsed < 150:
        text("1", 0, 0)
    else:
        text("GO!", -25, 0)
    
def checkCollisions():
    global mass, gameOver
    myX, myY = mouseX - w/2, mouseY - h/2
    for i in range(len(debrisX)):
        x, y, vx, vy, m = debrisX[i], debrisY[i], debrisVelX[i], debrisVelY[i], debrisMass[i]
        if dist(x, y, myX, myY) <= (mass + m)/2:
            if mass < m:
                gameOver = True
                text("GAME OVER", -100, 0)
                text("Click to try again!", -100, 50)
                noLoop()
            else:
                del debrisX[i], debrisY[i], debrisMass[i], debrisVelX[i], debrisVelY[i]
                mass += log(m)
            break
    for i in range(len(alienX)):
        x, y, m = alienX[i], alienY[i], alienMass[i]
        if dist(x, y, myX, myY) <= (mass + m)/2:
            if mass < m:
                text("GAME OVER", -100, 0)
                text("Click to try again!", -100, 50)
                noLoop()
            else:
                del alienX[i], alienY[i], alienMass[i]
                mass += log(m)
            break
      
def mousePressed():
    global gameOver
    if gameOver:
        gameOver = False
        restart()
        loop()

def restart():
    global levelNum, timeElapsed, numRed, numGold, maxMass, maxVel
    global mass, debrisX, debrisY, debrisVelX, debrisVelY, debrisMass
    global alienX, alienY, alienMass
    levelNum = 1
    timeElapsed = 0
    mass = 10
    numRed, numGold = 5, 0
    maxMass, maxVel = 15, 2.5
    alienVel = 1.0
    debrisX, debrisY, debrisVelX, debrisVelY, debrisMass = createDebris(numRed, maxMass, alienVel)
    alienX, alienY, alienMass = createAliens(numGold, maxMass)

def sign(a):
    return abs(a)/a if a != 0 else 0
    
