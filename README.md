
# Eat Em All

A Processing.py game where you progress through levels surrounded by enemies. You move with the mouse, grow by eating smaller enemies, and win a level only once all enemies have been devoured. At higher levels, larger enemies begin to chase you and smaller enemies flee. Can you beat Lvl 9?

## Gameplay

![](Media/level1.png)

![](Media/level2.png)

![](Media/level5.png)

![](Media/level6.png)